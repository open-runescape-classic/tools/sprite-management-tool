The sprite management tool may be used for importing sprites into the custom sprites cache in the custom client. Uses built-in Zulu OpenJDK 15 with OpenFX to launch. Currently windows-only.


**Getting started**

First, click "Unpack" and select the "Custom_Sprites.osar" cache file you wish to unpack. This is generally located in "Cache\video\Custom_Sprites.osar". 

Next, create a workspace folder or select an existing workspace folder for where to unpack "Custom_Sprites.osar" file. Give it a minute to unpack the archive before clicking "Open Workspace" and selecting the workspace folder where the sprites were unpacked.

The authentic sprites cache file requires the ancient sprite unpacker tool to extract and is out of scope for this tool's readme guide.


**Adding new sprites**

Create a new sprite by right-clicking on the left hand column where the sprite names are after ensuring the correct category has been picked. Click "New Entry", name it, and select the accurate "Entry Type" and "Slot" if it applies. If this does not work, simply select "Clone <name>" and modify that duplicate instead. 

In the center of the sprite management tool near the bottom, click the folder icon or the pencil icon depending if you want to import an array of animated sprites (NPC or player item) or a single sprite (texture, inventory item, etc.). Note how bluescale and grayscale affect the display. Using specific colors, this allows one sprite to have multiple colored instances. This is done with armour for each type that exists, same for dragons, worn items, and more. Animation type, slot, vertical/horizontal shift, and bound height/width are all related to worn items like weapons, hats, and possibly NPCs. By cloning similar existing sprites that have those already populated, it saves troubleshooting time.


**Saving changes**

Select the sprite category along the bottom row of the sprite management tool that you wish to access and then click the sprite's name (number or word) along the left hand side when it appears. When finished, click "Save Workspace" and "Pack Archive" to export the completed work. 

The sprite editor will prompt where to save the Custom_Sprites.osar cache file. Copy it to a client Cache folder, (preferably a local instance that isn't able to be overwritten by the PC Launcher) update how it is referenced in the server and client, and in theory, it should load properly.